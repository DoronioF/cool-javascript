import { IMPORTANT_VALUE, logInCaps, Dog } from './import-from-me.js';

function createTriviaHtml(category, question, answer) {
  return `
    <div>
      <h3>${category}</h3>
      <p><b>Question:</b> ${question}</p>
      <p><b>Answer:</b> ${answer}</p>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {
  console.log('DOM content fully loaded and parsed.');

  const url = "https://jservice.xyz/api/random-clue";
  try {
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data);

      const category = data.category.title;
      const question = data.question;
      const answer = data.answer;
      const html = createTriviaHtml(category, question, answer);

      const mainTag = document.querySelector('main');
      mainTag.innerHTML = html;
    } else {
      throw new Error('Response not ok');
    }
  } catch (error) {
    console.error('error', error);
  }
});

console.log('IMPORTANT_VALUE is', IMPORTANT_VALUE);
logInCaps('This is a message.');

const dog = new Dog('Fido');
const dogWords = dog.speak();
console.log(dogWords);

console.log('main.js is loaded');